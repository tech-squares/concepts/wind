\documentclass{article}
\usepackage{tex-squares}
\usepackage{fullpage}
\usepackage{hyperref}
\usepackage{tabularx}
\usepackage[table]{xcolor}
\usepackage{tablefootnote}
\usepackage{longtable}

\title{Wind}
\author{Dylan Hendrickson}
\date{\today}

\setlength{\parskip}{1em}

\begin{document}

\maketitle

\begin{abstract}
  We introduce the Wind concept for square dancing, which changes your winding number around the center of the set or of a subformation.
  A surprisingly large number of existing calls can be described simply in terms of Wind and other calls, and we expect that there are many other interesting and pleasant-to-dance instances of this concept.
\end{abstract}

\section{Motivating Examples}\label{sec:motivating}
  We begin by investigating several pairs of related calls.
  Ultimately, we wish to describe how all of these pairs are related, and this will become the Wind concept.%
  \footnote{Wind is pronounced as in ``wind the bobbin'', ``whined'', or ``winding number'', not a breeze.}

  \subsection{Acey Deucey vs. All 8 Circulate}
    From right-hand waves (and some other formations), the only difference between Acey Deucey and All 8 Circulate is the trailing centers' part, which is either flipping to the other center spot in their wave or walking straight to the other wave.

    \displaythree
      {\dancer 1n & \dancer 2s & \dancer 3n & \dancer 4s \\
       \dancer 8n & \dancer 7s & \dancer 6n & \dancer 5s}
      {before}
      {\dancer 8n & \dancer 3s & \dancer 2n & \dancer 1s \\
       \dancer 5n & \dancer 6s & \dancer 7n & \dancer 4s}
      {after Acey-Deucey}
      {\dancer 8n & \dancer 3s & \dancer 6n & \dancer 1s \\
       \dancer 5n & \dancer 2s & \dancer 7n & \dancer 4s}
      {after All 8 Circulate}

    The difference between these is essentially that the trailing centers swap placing with each other.

  \subsection{Trade vs. Box Circulate vs. Scoot Back}\label{sec:scoot back example}
    More generally, consider a miniwave box.
    Trade and Box Circulate are the centers' part of Acey Deucey and All 8 Circulate, so they're related in the same way: the trailers go to the opposite ending position, swapping with each other.

    Scoot Back has the same ending position as Trade, but the trailers go around each other to get there.
    You can think of them as aiming for the next `equivalent' spot clockwise (from a right-hand box), where we consider a position and its opposite to be equivalent.

    \displayfour
      {\dancer 1n & \dancer 2s \\
       \dancer 3n & \dancer 4s}
      {before}
      {\dancer 2n & \dancer 1s \\
       \dancer 4n & \dancer 3s}
      {after Trade}
      {\dancer 3n & \dancer 1s \\
       \dancer 4n & \dancer 2s}
      {after Box Circulate}
      {\dancer 2n & \dancer 1s \\
       \dancer 4n & \dancer 3s}
      {after Scoot Back}

    To summarize: to convert Trade to Box Circulate, we rotate the trailers' ending positions clockwise half way around the center of the formation.
    Applying this transformation again to Box Circulate gives Scoot Back, where the trailers must go around each other because the path has been `looped' around the center.

  \subsection{Straightfire}
    From two-faced lines, Straightfire as defined as Crossfire, but the trailing ends do a Crossover Circulate instead of a Cross Fold.

    \displaythree
      {\dancer 1n & \dancer 2n & \dancer 3s & \dancer 4s \\
       \dancer 5n & \dancer 6n & \dancer 7s & \dancer 8s}
      {before}
      {\dancer 3n & \dancer 1s \\
       \dancer 4n & \dancer 2s \\
       \dancer 7n & \dancer 5s \\
       \dancer 8n & \dancer 6s}
      {after Crossfire}
      {\dancer 3n & \dancer 1s \\
       \dancer 5n & \dancer 2s \\
       \dancer 7n & \dancer 4s \\
       \dancer 8n & \dancer 6s}
      {after Straightfire}

    Relative to Crossfire, this swaps the initial trailing ends.
    Straightfire is often more comfortable to dance, because the center isn't as crowded.%
    \footnote{One could also consider a flourish for Crossfire where the trailing ends go around each other in the middle, similar to Scoot Back; this would be another Wind of Straightfire.}

    This idea of replacing the trailing ends' Cross Fold or Cross Run with Crossover Circulate is generalized to the concept Straight.
    Straight almost always swaps the trailing ends relative to the original call.

    Unfortunately, Straight is limited to only this specific replacement.
    Wind is a generalization of Straight, and can be applied in many different situations.

  \subsection{Trade the Wave vs. Trade Circulate}
    One way Straight is limited is that it doesn't work `backwards'.
    Consider, from right-hand waves, Trade the Wave and Trade Circulate.
    The difference is whether the trailers cross run (staying in their own wave) or crossover circulate (into the other wave).

    \displaythree
      {\dancer 1n & \dancer 2s & \dancer 3n & \dancer 4s \\
       \dancer 5n & \dancer 6s & \dancer 7n & \dancer 8s}
      {before}
      {\dancer 3s & \dancer 4n & \dancer 1s & \dancer 2n \\
       \dancer 7s & \dancer 8n & \dancer 5s & \dancer 6n}
      {after Trade the Wave}
      {\dancer 3s & \dancer 5n & \dancer 1s & \dancer 7n \\
       \dancer 2s & \dancer 8n & \dancer 4s & \dancer 6n}
      {after Trade Circulate}

    The trailing ends' part can be described as Straight Trade the Wave, but Straight fails to capture the trailing centers' part.%
    \footnote{The enterprising caller may notice that the trailing centers' part is the same as in Rewind Straight Rewind Trade the Wave, so from this formation Trade Circulate is the same as Straight Rewind Straight Rewind Trade the Wave.}
    But there isn't a fundamental difference, between dancers, of the relationship between a trailer's part in Trade the Wave and the same dancer's part in Trade Circulate.
    They all end up swapped with their opposite, and we would like a single concept that captures this.

  \subsection{Lead Right, etc.}
    Finally, consider the call Lead Right.
    Suppose we modify the call by having the beaus swap by taking each others' spots, with their new paths going clockwise relative to their paths in Lead Right.
    The resulting call is Pair Off.

    We can do this again: have the beaus swap again by again taking each others' spots, working clockwise.
    Now the call ends in the same position as Lead Right, but the Beaus pass each other clockwise around the center (passing right shoulders).
    This is Wheel Thru.

    And once more, increase the beaus' winding number around the center by a half, swapping them with each other.
    Now they end in the same positions as after Pair Off, but wind all the way around each other.
    This is called Wheel Fan Thru.

    \displayfive
      {\dancer 1s & \dancer 2s \\
       \dancer 3n & \dancer 4n}
      {before}
      {\dancer 1w & \dancer 3e \\
       \dancer 2w & \dancer 4e}
      {after Lead Right}
      {\dancer 1w & \dancer 2e \\
       \dancer 3w & \dancer 4e}
      {after Pair Off}
      {\dancer 1w & \dancer 3e \\
       \dancer 2w & \dancer 4e}
      {after Wheel Thru}
      {\dancer 1w & \dancer 2e \\
       \dancer 3w & \dancer 4e}
      {after Wheel Fan Thru}

\section{Defining Wind}
  In this section, we attempt to carefully define Wind.
  In order to Wind, a dancer must be told a winding direction (either clockwise or counterclockwise) and a winding center (which is a point on the floor). We describe ways the caller can convey these in Section~\ref{sec:calling} and what the defaults are if the caller doesn't specify, but for now assume them as given.

  The result of winding is that the dancer is swapped with their opposite relative to the winding center---in other words, their final position is rotated 180$^\circ$ around the winding center. The winding direction determines which direction around the winding center they go to get there.

  \subsection{When to Wind}
    When, and how, should winding dancers swap places? 
    The rule we have settled on, which seems to work well for most calls, is that dancers told to Wind should perform the winding action the first time they get as close to the center as they get throughout the call.

    On ``Trailers Wind Box Circulate'' (i.e. Scoot Back), this is halfway through, after the trailers Extend.

  \subsection{How to Wind}
    The actual dance action depends on whether a dancer's facing direction when they Wind agrees with their winding direction.
    If it does, they can simply Counter Rotate 1/2.

    For instance, consider Wheel Thru from facing couples, where the beaus are told to Wind clockwise  around the center of the box.
    The beaus are closest to the center halfway through, when the formation is a diagonal left-hand wave. At that time, they Counter Rotate 1/2, which is equivalent to them trading, and then everyone does the second half of the Wheel Thru. So this application of Wind can be described as ``Wheel Thru 1/2; Slip; second half of Wheel Thru'' (or ``Sandwich Wheel Thru Around Slip''), which is equivalent to Wheel Fan Thru. 

    If a dancer is facing opposite their Wind direction when they're ready to Wind, it's a bit more awkward.
    In this case, the dancer should U-Turn Back inwards, Counter Rotate 1/2, and U-Turn Back outwards.
    The dancer turns the same direction on both U-Turn Backs, and this is the opposite of the direction they're winding.
    This definition may seem arbitrary, but it seems to work well on all the examples we've considered, and in particular doesn't introduce full 360$^\circ$ turns to dancers' paths.

    For instance, consider Trade from a right-hand box, and suppose the trailers are asked to Wind clockwise around the center of the box.
    The trailer's part is then Hinge, U-Turn Back left, Counter Rotate 1/2, U-Turn Back left, and Hinge. 

  \subsection{Equivalent Paths}\label{sec:homotopy}
    By the definition described above, it is not fun to Wind opposite the direction you're facing when you're closest to the center.
    To fix this, the Wind concept also asks dancers to shortcut, as long as they only affect their own path, and not how they move around other dancers or their winding center.

    If two traffic patterns can be related by changing individual dancers' paths, without changing how dancers move around each other or the center, we say the traffic patterns are \emph{equivalent}.%
    \footnote{For math nerds: this is homotopy with fixed endpoints in the space of dancer configurations.}
    You can think about each dancer connecting one end of a ribbon to the floor where they start, and holding the other end as they dance the call (stepping over any ribbons they encounter).
    Two traffic patterns are equivalent if the final ribbon arrangements can be rearranged into each other without passing ribbons through each other or having dancers step over or under ribbons.

    In the ribbon-based view of equivalence, a dancer spinning 360$\circ$ appears as their ribbon being twisted; they need to lift it over their head to avoid being caught on it. In particular, we do not consider paths to be equivalent if they differ by a full twirl.

    Consider again Trade from a right-hand box, with the trailing winding clockwise around its center.
    In Section~\ref{sec:scoot back example}, we claimed that this call should be Circulate, so the trailer's part should just be walking forwards, which is much more pleasant than what we computed in the previous subsection.
    However, these two paths are equivalent: the trailer is making a zigzagging path which can be `pulled tight' to a straight line---they don't cross any other dancers' paths, and turn left and right the same amount.%
    \footnote{Some earlier attempts at the definition of Wind had this dancer turn 360$^\circ$ to the right while circulating, which is not equivalent; we believe the definition presented never does this.}

  \subsection{Flow-Dependent Calls}
    Because the traffic pattern is defined only up to equivalence, the flow after a call involving Wind is not generally well-defined.
    We advise caution in following a call containing Wind with any call the depends on flow, such as Roll, Sweep a Quarter, or With the Flow.

\section{Calling Conventions}\label{sec:calling}
  In this section, we provide ways the caller can tell the dancers how to Wind.
  The primary goal is to successfully communicate who should Wind, what direction they should Wind, and what point they should Wind around.

  \subsection{Who Winds}
    Very rarely does the caller want everybody to Wind.
    So the caller should always designate some people before saying Wind, such as ``Trailers Wind''.
    If the caller does want everybody to Wind, they should be explicit by saying ``Everybody Wind''.

  \subsection{Winding Direction}
    By default, a dancer's Wind direction is determined by their facing direction at the start of the call, in the same way as Counter Rotate. This may or may not be the same as their orientation when they actually perform the Wind.
    This appears to be the most convenient convention, and makes all of the examples of Section~\ref{sec:motivating} easy to say.
    For instance, on Trailers Wind Trade the Wave from right-hand waves--which is equivalent to Trade Circulate--the ends Wind clockwise while the centers Wind counterclockwise.
    Many alternative conventions would have them Wind in the same direction, which makes for a worse call in this case.

    For cases where the default direction isn't meaningful (since the dancer is facing directly in or out) or isn't desired, we suggest a few ways the caller can specify the Winding direction.

    \begin{itemize}
      \item To Wind opposite the initial direction, the caller can say ``Unwind''.
            Unwind and Wind precisely undo each other.
      \item To Wind in a specific direction, the caller can say ``Clockwise Wind'' or ``Counterclockwise Wind''. The former can be shortened to ``Clockwind''. Another word for counterclockwise is `widdershins', so the latter can also be pronounced ``Windershins''.
      \item There may be cases that aren't directly covered by these.
            In such cases, the caller should just specify the direction to (groups of) dancers individually.%
            \footnote{In the special case where the caller wants dancers to Wind in a direction based on their facing direction at the \emph{end} of the call, they can say ``Rewind Wind Rewind'' (or ``Rewind Unwind Rewind'') for extra excitement.}
    \end{itemize}

  \subsection{Winding Center}
    By default, dancers told to Wind will Wind around the center of the entire set, even if the original call is done in smaller formations.
    For instance, from waves, Trailers Wind Trade has all the trailers try to barge through each other in the center, and isn't recommended.
    The caller likely meant to say Trailers Split Wind Trade, which is equivalent to Scoot Back.

    There may be many ways to say the same thing---in this example ``Trailers Split Wind Trade'', ``Trailers Box Wind Trade'', and ``Each Box, Trailers Wind Trade'' would likely all be interpreted the same way.

    In general, there are a few ways the caller can specify what to Wind around.

    \begin{itemize}
      \item ``Split Wind'' means to split the 8-person formation into two halves in the long direction, and Wind in each one, similar to other uses of Split.
      \item ``Single Wind'' means to wind relative to your $1\times 4$ subformation in a  $2\times 4$, similar to e.g. ``Single Sidetrack''.
      \item To Wind relative to some subformation, the caller can say ``[somebody] [formation] Wind''.
      \item Alternatively, the caller can say ``Each [formation], [somebody] Wind''.
    \end{itemize}

    Let's see a few more examples.
    \begin{itemize}
      \item From waves, ``Trailers Split Wind All 8 Circulate'' is equivalent to Scatter Scoot. Note that Split Wind can be called even on an 8-dancer call; this requires that the dancers Winding stay in their own subformation.
      \item From any formation with facing dancers, ``Each Facing Pair Everybody Clockwind Pass Thru'' is equivalent to Turn Thru. Note that the dancers are winding relative to the center of their two-person subformation.
    \end{itemize}

  \subsection{Winding Amount}
    As described so far, the Wind concept always has a dancer Wind halfway around the center, swapping them with their opposite.
    Occasionally, the caller may want dancers to Wind a different amount, which can be described by putting a multiplier on Wind. This multiplier is applied to the ``1/2'' in the ``Counter Rotate 1/2'' in the definition of Wind.

    For instance, Beaus Double Wind Lead Right is Wheel Thru, and Beaus Triple Wind Lead Right is Wheel Fan Thru.

    The multiplier doesn't need to be an integer! Everybody Half Wind Do Nothing is Counter Rotate.
    For a more interesting example, from back-to-back couples, Mini Chase can be called as Everybody Half Windershins Chase Right.%
    \footnote{Or Rewind Everybody Half Unwind Rewind Chase Right}.

\section{When Wind is Good}
  How can a caller tell whether applying Wind will result in a call that's comfortable to dance? In this section, we provide some heuristics and examples, but none of this is necessarily universal.

  For most short calls, trailers can comfortably Wind, but leaders would have to move unreasonably far to Wind.
  For longer calls, dancers move around enough that most dancers probably have a good chance to Wind.

  It is easier to Wind when you're close to the point you would Wind around (the center of the set or of a subformation).
  So Winding is usually more comfortable for dancers who are already getting very close to the center on the call.
  This is especially true if no other dancers are in the center of the formation at the same time, so the Winders have plenty of space to trade.
  Sometimes it may even be more comfortable for dancers to Double Wind a call than to dance the original call; e.g. Wheel Thru has more space to move than Lead Right.
  Dancers often do flourishes in which the (temporary) very centers go past each other, which amounts to a Double Wind, such as on Criss Cross Your Neighbor.

  If dancers do get close to the center, often either direction of Winding is comfortable.
  But be wary of overflow---Beaus Wind Wheel Fan Thru is a bit much.

  \subsection{Common Path Conversions}
    A large fraction of applications of Wind modify the same few kinds of paths.
    Generally, applications that modify these same paths will make for good dancing.
    Memorizing these may also be useful for dancers learning Wind.
    \begin{itemize}
      \item Flipping towards the center $\longrightarrow$ Walking forward
      \item Walking forward $\longrightarrow$ Trading with your opposite (like Scoot Back)
      \item Turning around $\longrightarrow$ Walking past the center
      \item Turning away from center $\longrightarrow$ Going around and past the center
    \end{itemize}
    Of course, Unwind modifies paths in exactly the opposite direction.

  \subsection{New Examples}
    In Appendix~\ref{app:examples}, we list many existing calls which can be defined using Wind in terms of other calls.
    In this section, we provide several instances of Wind which are not equivalent to any existing call (that we know of).
    These can serve as inspiration to callers looking to include Wind in their choreography.
    
    \begin{itemize}
      % plus
      \item From facing couples: Everybody Clockwind Pass Thru. The beaus do their part of Scoot Back while the belles do their part of swap around.
      \item From two-faced lines: Trailers Wind Wheel and Deal. The trailing couple walks forward to the far box. The Straight concept could get the trailing end to do their part of this call, but doesn't apply to the trailing center.
      \item From Right-Hand Waves: Inner Actives Clockwind Recycle. This is similar to the above: the leading center does a U-Turn Back and then an As Couples Extend and Veer Right with their partner.
      \item From right-hand waves: Inner Actives Clockwind Linear Cycle. The inner actives' part is Centers Fold; Double Pass Thru; Face In. Alternatively, you can think of it as the call in the previous bullet followed by a clockwise Sweep 1/4.
      \item From Waves: Trailing Centers Wind All 8 Circulate. This can also be pronounced ``Lines Scoot Back Thru''.
      \item From right-hand waves: Trailers Clockwind Trade. The Trailers' part is the same as in Bias Circulate; this could reasonably be described as ``Bias Split Circulate''.
      \item From an eight chain: End Belles Wind Right and Left Thru. The beau going into the center turns the person in front of them instead of the person next to them.
      \item From a tidal wave: \#2s from the end Unwind Linear Cycle. Instead of peeling at the end of Linear Cycle, the dancers asked to Unwind walk forward into to other box.
      \item From normal facing lines, with flow such that the Sweep is clockwise: Center Boys Unwind Sweep a Quarter. The Center Boys do their part of Centers Partner Tag.
      \item From an eight chain: End Beaus Wind Flutterwheel. The dancers ask to Wind go across to the other box on their way in, letting go of their partner.
      \item From waves: Centers Unwind Spin Chain Thru. This is ``Spin Chain Thru, Ends Circulate Twice''.
      \item From waves: Trailing Centers Wind Walk and Dodge. The trailing centers' part is a Scoot Back. If instead they were asked to Unwind, they would do a Run in the center.
      \item From an Eight Chain: Ends Clockwind Circle to a Line. After each side circles 1/4, the (current) centers drop hands and everybody circles left about 1/4, adjusting into the end of Circle to a Line.
      % a1
      \item From an eight chain: End Beaus Wind Wheel Thru. The end beaus do two circulates and face left; they should go behind the belle they start out facing.
      \item From a single starting double pass thru: Centers Clockwind Right Roll to a Wave. This is the same as Centers Pass Thru, Touch.
      \item From waves, or any lines though the traffic pattern may be worse: Trailing Centers Wind Crossover Circulate. The trailing centers' part is to scoot back (with each other) and spread.
      % a2
      \item From waves: Trailers Split Clockwind Trade Circulate. The trailers a Jay Turn Thru.
      \item From a right-hand box: Leaders Wind Remake. Those dancers trade when they're in the center; it's equivalent to Thirdly Stingy Swing the Fractions.
      \item From right-hand waves: Trailing Ends Wind Remake. They trade when they're in the very center, equivalent to Hinge by 1/4 by 1/4 by 1/2 by 1/4 by 3/4.
      \item From a tidal single eighth chain: \#3s from the end Clockwind Pass and Roll. Those dancers Pass Thru with each other instead of U-Turn Back. If instead asked to Double Clockwind, they would turn thru with each other, like Initially Grand Working Forwards Pass and Roll.
      % c1
      \item From waves: Trailing Centers Unwind Follow Thru. The trailing centers' part is essentially Centers Partner Tag.
      % c2
      \item From a miniwave box: Trailers Wind Cross Back. This is like Scoot Back, except that the first Extend is a Cross Extend.
      \item From columns: \#4s Wind Walk Out to a Wave. The \#4s do their part of Coordinate.
      % c3a
      \item From columns: Leads Wind Wind the Bobbin. The leads' part is just the initial peel off, without the circulates.
      \item From a squared set: Head Boys Unwind Heads Spin the Pulley. This replaces the initial Touch 3/4 with Swing Thru 1 1/4.
    \end{itemize}

\section{Using Wind to Resolve}
  When writing material, Wind can be a helpful tool for permuting dancers. When, at the end of a sequence, you have a nice resolution but with the side girls crossed, a common strategy is to add Side Girls Chain to the beginning of the sequence. Wind provides another, more interesting solution: ask the side girls to Wind on any call in the sequence, ideally one for which that's comfortable.

  This idea may be useful even for a caller who doesn't want to put Wind in their choreography. You probably already know that, from waves, replacing Circulate with Acey Deucey has the net effect of swapping the trailing centers. Wind generalizes this pair, and is helpful for identifying other replacements to make in order to swap two dancers. If the crossed side girls happen to be leading ends in a Criss Cross the Deucey somewhere in the sequence, you can replace it with Criss Cross Your Neighbor to swap them. The examples in Appendix~\ref{app:examples} may be helpful when using this strategy.

\section*{Acknowledgements}
  From its original conception to the writing of this paper, many people have helped with development of Wind, including workshopping the definition, finding examples, reassuring me that Wind is a worthwhile concept, and providing feedback on drafts of this paper. These people include
  Alex Dehnert,
  Andy Latto,
  Andy Tockman,
  Emily Crandall-Fleischman,
  Helen Read,
  Jack Gurev,
  Josh Brunner,
  Justin Kopinsky,
  Linus Hamilton,
  Lotta Blumberg,
  Luke Sciarappa,
  Pi Fisher,
  Veronica Boyce,
  and likely others I am forgetting.

\appendix

\section{New Definitions Using Wind}\label{app:examples}
  In this appendix, we list many pairs of existing calls which are related by Wind, including the examples from Section~\ref{sec:motivating}. We present each pair as a definition of the higher-level call using Wind and the other call. Many of these only work in some formations; we describe one such formation (or family of formations), and trust the reader to generalize as desired.
  The definitions give the correct paths (in the sense described in Section~\ref{sec:homotopy}), but do not necessarily fractionalize the same, have the same roll direction (to the extent that's defined), etc.
  Unless otherwise specified, the Wind is around the center of the formation described; a caller may need to say e.g. ``Split Wind'' to achieve this.
  This list is not comprehensive, and we would appreciate hearing about any other interesting examples you discover.

  The examples are sorted in related groups and by level, though we don't bother distinguishing between levels below Plus.


  \begin{center}
    \rowcolors{2}{gray!25}{white}
    \renewcommand{\arraystretch}{1.2}

    \begin{longtable}{|c|>{\raggedright}m{3cm}c|>{\raggedright}m{5.4cm}c|} \hline
      \rowcolor{gray!50}
      Start formation & Call & Level & New definition & Level \\ \hline
      \endhead
      %% copied from spreadsheet
        Right-Hand Waves & Bias Circulate & C3A & Trailers Clockwind Outroll Circulate & A2 \\ \hline
        Lines & Zoom Roll Circulate & C4 & Leading Ends Unwind Outroll Circulate & A2 \\ \hline
        Lines Facing Out & Roll Em & C4 & Judges Unwind Here Comes the Judge & C2 \\ \hline
        Miniwave Box & Walk And Dodge & Plus & Trailers Wind Trailers Run & Plus \\ \hline
        Miniwave Box & Scoot And Dodge & A1 & Trailers Double Wind Trailers Run & Plus \\ \hline
        Waves & Crossover Circulate & A1 & Trailers Wind Trade the Wave & Plus \\ \hline
        Waves & Trade the Deucey & C3A & Trailing Ends Wind Trade the Wave & Plus \\ \hline
        Waves & Orbit Circulate & C4 & Leader On Right Unwind Trade Circulate & A2 \\ \hline
        Waves & Twin Orbit Circulate & C4 & Leaders Unwind Trade Circulate & A2 \\ \hline
        Line & Cross Roll & C1 & Centers Wind Ends Run & Plus \\ \hline
        Line & Switch the Line & C1 & Ends Wind Centers Run & Plus \\ \hline
        Back-to-Back Couples & Mini Chase & C3A & Beaus Wind Latch On & C3A \\ \hline
        Facing Couples & Short Cut & C4 & Belles Unwind Touch 1/4 & Plus \\ \hline
        Any 2x2 & Short and Sweet & C4 & Leading Beaus and Trailing Belles Clockwind Latch On & C3A \\ \hline
        Back-to-Back Couples & Shakedown & C1 & Belles Unwind Trade And Roll & Plus \\ \hline
        Facing Couples & Split Swap & C2 & Belle Wind Partner Tag & A1 \\ \hline
        Any 2x2 & Shake and Rattle & C3B & Leading Belles and Trailing Beaus Clockwind Leaders Kick Off & C2 \\ \hline
        Any Box & Box Circulate & Plus & Trailers Wind Trade & Plus \\ \hline
        Miniwave Box & Scoot Back & Plus & Trailers Double Wind Trade & Plus \\ \hline
        Miniwave Box & Zoom & Plus & Trailers Wind Leaders Unwind Trade & Plus \\ \hline
        Miniwave Box & Boomerang & C3B & Trailers Double Wind Leaders Unwind Trade & Plus \\ \hline
        Facing Couples & Fan Thru & C4 & Belles Wind Pass Thru & Plus \\ \hline
        Facing Dancers & Pass Thru & Plus & Everybody Clockwind U-Turn Back & Plus \\ \hline
        Facing Dancers & Turn Thru & Plus & Everybody Double Clockwind U-Turn Back & Plus \\ \hline
        Facing Couples & Central Finish Snap the Lock & C3A & Everybody Half Wind Pass Thru & Plus \\ \hline
        Facing Couples & Pass Out & A1 & Everybody Wind Partner Tag & A1 \\ \hline
        Trade By & Hit the Wall & C4 & Centers Wind Partner Tag & A1 \\ \hline
        Facing Couples & Pair Off & A1 & Beaus Wind Lead Right & Plus \\ \hline
        Facing Couples & Wheel Thru & A1 & Beaus Double Wind Lead Right & Plus \\ \hline
        Facing Couples & Wheel Fan Thru & C1 & Beaus Triple Wind Lead Right & Plus \\ \hline
        Waves & In Roll Circulate & A2 & Trailing Centers Unwind Split Circulate & Plus \\ \hline
        Columns & Triple Scoot & Plus & Trailing Centers Double Wind Scoot Back & Plus \\ \hline
        RH Tidal Wave & Grand Swing Thru & Plus & End Centers Unwind Swing Thru & Plus \\ \hline
        LH Tidal Wave & Grand Swing Thru & Plus & Very Centers Wind Swing Thru & Plus \\ \hline
        Columns & Grand Follow Your Neighbor & A1 & Trailing Centers Double Wind Follow Your Neighbor & Plus \\ \hline
        Two-Faced Lines & Ease Off & C3A & Trailers Wind Leading Ends Unwind Couples Trade and Roll & Plus \\ \hline
        Two-Faced Lines & Nice And Easy & C4 & Trailing Ends Wind Leading Ends Unwind Couples Trade and Roll & Plus \\ \hline
        Two-Faced Lines & Quick Change & C4 & Trailing Ends Wind Couples Trade and Roll & Plus \\ \hline
        Lines & Ends Cut In & C4 & Trailing Ends Wind Switch & A2 \\ \hline
        Lines & Centers Cut Out & C4 & Trailing Centers Wind Cross Roll & C1 \\ \hline
        Two-Faced Lines & Gee Whiz & C3B & Trailers Wind Crossfire & Plus \\ \hline
        Miniwave Box & Cross and Turn & C1 & Beaus Wind U-Turn Back & Plus \\ \hline
        Miniwave Box & Cross Back & C2 & Trailers Unwind U-Turn Back & Plus \\ \hline
        Facing Couples & Cross Trail Thru & A1 & Everybody Wind [Turn Thru Or U-Turn Back] & Plus \\ \hline
        Facing Couples & Fan And Cross Thru & C4 & Belles Wind Cross Trail Thru & A1 \\ \hline
        Waves & Criss Cross Your Neighbor\footnotemark & C2 & Leading Ends Double Wind Criss Cross Your Neighbor & C2 \\ \hline
        Waves & Criss Cross the Deucey & C3B & Leading Ends Wind Criss Cross Your Neighbor & C2 \\ \hline
        Back-to-Back Couples & Mini Chase & C3A & Half Windershins Chase Right & Plus \\ \hline
        Lines & Acey Deucey & Plus & Trailing Centers Wind All 8 Circulate & Plus \\ \hline
        Waves & Outroll Circulate & A2 & Trailers Split Unwind All 8 Circulate & Plus \\ \hline
        Waves & Scatter Scoot & C1 & Trailers Split Wind All 8 Circulate & Plus \\ \hline
        Columns & Column Circulate & Plus & Trailing Centers Wind Split Circulate & Plus \\ \hline
        Facing Couples & Pass the Ocean & Plus & Everybody Wind Partner Trade 1 1/2 & Plus \\ \hline
        RH Wave & Linear Cycle & Plus & Everybody Half Clockwind Recycle & Plus \\ \hline
        Facing Couples & Swap Around & A1 & Beaus Unwind Pass Thru & Plus \\ \hline
        Wave & Lockit & A1 & Centers Unwind Fan the Top & Plus \\ \hline
        Two-Faced Lines & Trade Circulate & A2 & Trailing Ends Wind Trailing Centers Unwind Trade & Plus \\ \hline
        Miniwave Box & Box Transfer & A2 & Half Wind Scoot Back & Plus \\ \hline
        Waves & Relay the Top & C1 & Trailing Ends Wind Leading Centers Unwind Spin the Top Twice & Plus \\ \hline
        Any & Counter Rotate & C1 & Half Wind Nothing & Plus \\ \hline
        Miniwave Box & Zing & C1 & Trailers Wind Leaders Unwind Trade and Roll & Plus \\ \hline
        Trade By & Wheel And & C1 & Belle Wind Clover And & A1 \\ \hline
        Hourglass & 3 By 2 Acey Deucey & C1 & Trailing Ends Unwind 6 By 2 Acey Deucey & A1 \\ \hline
        Columns & Finish Coordinate & C1 & Trailing Centers Double Wind Trail Off & A2 \\ \hline
        Columns & Checkover & C1 & \#3s Double Wind Transfer the Column And Slither & A2 \\ \hline
        Eight Chain & Cross Chain And Roll & C1 & Ends Windershins Rotary Spin & C1 \\ \hline
        Miniwave Box & Shazam & C2 & Trailers Double Unwind Follow Your Neighbor & Plus \\ \hline
        Any & Relocate & C2 & Very Centers Wind Counter Rotate & C1 \\ \hline
        Miniwave Box & Single Checkmate & C3A & Half Wind Box Circulate & Plus \\ \hline
        Right-Hand Waves & Swing Chain Thru & C3A & Leading Ends Wind Swing Thru & Plus \\ \hline
        Facing Couples & Swap the Top & C3A & Everybody Windershins Pass the Ocean & Plus \\ \hline
        Miniwave Box & Follow to a Diamond & C3A & Trailers Wind Circulate 1 1/2 & Plus \\ \hline
        3/4 Tag & Delight/Dilemma & C3A & Secondly Very Centers Wind Spin the Windmill Right/Left & A2 \\ \hline
        Back-to-Back Couples & Chase Back & C3B & Beaus Double Wind Chase Right & Plus \\ \hline
        Eight Chain & Square Out & C4 & Ends Wind Square Thru & Plus \\ \hline
        Two-Faced Lines & Finish Explosion & C4 & Trailing Centers Wind Couples Circulate & Plus \\ \hline
        Two-Faced Lines & Spin A Wheel & C4 & Trailing Ends Unwind Centers Single Wind Couples Trade & Plus \\ \hline
        Columns & Straight Fire & C4 & Trailing Ends Wind Crossfire & Plus \\ \hline
        LH Two-Faced Lines & Splash Out & C4 & Trailers Wind Explode the Line & A1 \\ \hline
        Any 2x2 & Central Bail Out & C4 & Leading Belles and Trailing Beaus Clockwind Central Team Up; Circulate & C3A \\ \hline
        Waves & Wipe Out & C4 & Trailing Centers Double Wind Rewind Curl Apart & C4 \\ \hline

    \end{longtable}
    \footnotetext{With the flourish where the leading ends go around each other in the center.}
  \end{center}

\section{Nonstandard Geometry}
  For dancers familiar with bigons or apeirogons, it may be instructive to consider what wind does in these extremal nonstandard geometries.
  The relationships between the calls in the examples of Section~\ref{sec:motivating} become especially clear by considering what happens in a bigon or an apeirogon.

  \subsection{Bigons}
    In a bigon, in each of the examples we've considered, all of the calls end in the same position.

    % todo: bigon pictures of a few of them

    Then what is different between these calls? The only difference is the dancers' winding numbers around the center of the set.
    Sometimes this is just a matter of which side of the center the dancer went on.

    From a (bigon) right-hand box, on Trade, the trailer goes on the right side of the center.
    On Box Circulate, the trailer goes around the left side of the center.
    On Scoot Back, the trailer goes around the left side with an extra lap all the way around.
    The leader's part is always the same.

    From (bigon) facing couples, on Lead Right, the beau steps around the center to the right, holding the belle's hand.
    On Pair Off, the beau walks around to the left.
    On Wheel Thru, they go to the left and all the way around.
    On Wheel Fan Thru, they go to the left and all the way around a full two times.

    If you've ever danced a bigon and cheated by going on the wrong side of the center, you've danced Wind.
    For instance, on Trade the Wave, if the trailers use the far side of the center because there's limited space on the close side, they dance a Trade Circulate instead.
    Callers can use such cheats to come up with novel danceable applications of Wind---watch how lazy bigon dancers perform a call, and tell dancers in a square to apply the same winding.

    In a bigon, Wind simply increments a dancer's winding number around the center, without changing their final position.

  \subsection{Apeirogons}
    Geometry with more dancers than a square, such as hexagons, octagons, etc., reveals dancers' winding numbers more clearly.
    The extreme version is an apeirogon, where no finite winding number around the center of the set returns a dancer to their starting position.

    In an apeirogon, incrementing a dancer's winding number (as counted in a bigon) moves the dancer one formation over; that dancer's infinitely many opposites all shift one position over.

    While in a square, Lead Right and Wheel Thru end in the same position, as do Pair Off and Wheel Fan Thru, apeirogons lets us distinguish between all of these.%
    \footnote{Octagons are big enough to distinguish between these four calls, but apeirogons are more general, and can distinguish between any number of winds.}
    In an apeirogon, each of these calls puts a beau with a different one of their partner's opposites.

    % todo: apeirogon pictures

    In an apeirogon, Wind moves a dancer's final position by one formation in the appropriate direction, to where their nearest opposite is.


\end{document}
